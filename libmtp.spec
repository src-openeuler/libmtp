Name:           libmtp
Version:        1.1.22
Release:        2
Summary:        An Initiator implementation of the Media Transfer Protocol (MTP) in the form of a library
License:        LGPL-2.1-or-later
URL:            https://libmtp.sourceforge.net/
Source0:        https://download.sourceforge.net/%{name}/%{name}-%{version}.tar.gz
Patch0:         0001-doc-Don-t-document-internal-endian-macros.patch
Patch1:         libmtp-1.1.22-udev-dir.patch

BuildRequires:  autoconf automake libtool
BuildRequires:  libusbx-devel doxygen libgcrypt-devel gcc gettext-devel
Requires:       udev
Provides:       %{name}-examples%{?_isa} %{name}-examples
Obsoletes:      %{name}-examples

%description
libmtp is an Initiator implementation of the Media Transfer Protocol (MTP)
in the form of a library suitable primarily for POSIX compliant operating systems.
We implement MTP Basic, the stuff proposed for standardization.

%package devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}
Requires:       pkgconfig, libusb1-devel, libgcrypt-devel

%description devel
This package includes development files for libmtp.

%prep
%autosetup -n libmtp-%{version} -p1

%build
autoreconf -fi
%configure --disable-static --with-udev-rules=69-libmtp.rules --with-udev=%{_prefix}/lib/udev
%make_build

%install
%make_install
%delete_la
rm -f $RPM_BUILD_ROOT%{_bindir}/mtp-delfile
rm -f $RPM_BUILD_ROOT%{_bindir}/mtp-getfile
rm -f $RPM_BUILD_ROOT%{_bindir}/mtp-newfolder
rm -f $RPM_BUILD_ROOT%{_bindir}/mtp-sendfile
rm -f $RPM_BUILD_ROOT%{_bindir}/mtp-sendtr
pushd $RPM_BUILD_ROOT%{_bindir}
ln -sf mtp-connect mtp-delfile
ln -sf mtp-connect mtp-getfile
ln -sf mtp-connect mtp-newfolder
ln -sf mtp-connect mtp-sendfile
ln -sf mtp-connect mtp-sendtr
popd
iconv -f iso-8859-1 -t utf-8 -o COPYING.utf8 COPYING
touch -r COPYING COPYING.utf8; mv -f COPYING.utf8 COPYING
mkdir -p -m 755 $RPM_BUILD_ROOT%{_pkgdocdir}
install -p -m 644 AUTHORS README TODO \
      $RPM_BUILD_ROOT%{_pkgdocdir}
if [ $RPM_BUILD_ROOT/usr/share/doc/%{name}-%{version}/html != $RPM_BUILD_ROOT%{_pkgdocdir}/html ] ; then \
	mv $RPM_BUILD_ROOT/usr/share/doc/%{name}-%{version}/html \
		$RPM_BUILD_ROOT%{_pkgdocdir} ; \
fi
touch -r configure.ac \
      $RPM_BUILD_ROOT%{_includedir}/*.h \
      $RPM_BUILD_ROOT%{_libdir}/pkgconfig/*.pc

%files
%license COPYING
%{_libdir}/%{name}.so.9*
%{_udevrulesdir}/69-%{name}.rules
%{_udevhwdbdir}/69-%{name}.hwdb
%{_prefix}/lib/udev/mtp-probe
%{_bindir}/mtp-*

%files devel
%{_libdir}/%{name}.so
%{_pkgdocdir}
%{_includedir}/%{name}.h
%{_libdir}/pkgconfig/%{name}.pc

%changelog
* Fri Jan 17 2025 zhangshaoning <zhangshaoning@uniontech.com> - 1.1.22-2
- Fix bad date in changelog

* Thu Nov 21 2024 Funda Wang <fundawang@yeah.net> - 1.1.22-1
- Upgrade to 1.1.22

* Fri Apr 28 2023 wangkai <13474090681@163.com> - 1.1.21-1
- Upgrade to 1.1.21

* Mon Mar 28 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 1.1.19-1
- Upgrade to 1.1.19

* Wed Aug 04 2021 Liu Yinsi <liuyinsi@163.com> - 1.1.14-7
- define use_mtpz only in one .c file

* Tue Jun 08 2021 wulei <wulei80@huawei.com> - 1.1.14-6
- fixes failed: error: no acceptable C compiler found in $PATH

* Fri Nov 29 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.1.14-5
- Package init
